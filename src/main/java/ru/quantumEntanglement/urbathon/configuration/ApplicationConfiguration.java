package ru.quantumEntanglement.urbathon.configuration;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import ru.quantumEntanglement.urbathon.security.JwtTokenFilter;
import ru.quantumEntanglement.urbathon.security.JwtTokenProvider;

@Configuration
@EnableWebSecurity
@EnableMethodSecurity
@RequiredArgsConstructor(onConstructor = @__(@Lazy))
public class ApplicationConfiguration {
    private final JwtTokenProvider tokenProvider;
    /**
     * Создает экземпляр класса BCryptPasswordEncoder для использования в приложении.
     *
     * @return BCryptPasswordEncoder, используемый для хеширования паролей
     */

    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }
    /**
     * Создает AuthenticationManager для использования в процессе аутентификации.
     *
     * @return AuthenticationManager, используемый в приложении
     * @throws Exception, если возникает ошибка при создании AuthenticationManager
     */

    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration configuration) throws Exception{
        return configuration.getAuthenticationManager();
    }
    /**
     * Создает и настраивает экземпляр OpenAPI для API LETI магазина товаров.
     *
     * @return Экземпляр OpenAPI с настройками безопасности, компонентами и информацией API.
     */

    @Bean
    public OpenAPI openAPI(){
        return new OpenAPI()
                .addSecurityItem(new SecurityRequirement().addList("bearerAuth"))
                .components(
                        new Components()
                                .addSecuritySchemes("bearerAuth",
                                        new SecurityScheme()
                                                .type(SecurityScheme.Type.HTTP)
                                                .scheme("bearer")
                                                .bearerFormat("JWT")
                                )
                )
                .info(new Info()
                        .title("URBATHON -> проект по автоматизации улучшения городской среды")
                        .description("SPRING -> Vue 3")
                        .contact(new Contact()
                                .name("Владислав")
                                .url("https://t.me/ProstoVladTut")
                                .email("ooovladislavchik@gmail.com")));
    }
    @Bean
    public SecurityFilterChain filterChain(HttpSecurity httpSecurity) throws Exception{
        httpSecurity
                .csrf(AbstractHttpConfigurer::disable)
                .cors(cors -> {
                    cors.configurationSource(request -> {
                        CorsConfiguration corsConfig = new CorsConfiguration();
                        corsConfig.addAllowedOrigin("http://localhost:5173");
                        corsConfig.addAllowedOrigin("http://172.21.48.1:5173");
                        corsConfig.addAllowedOrigin("http://192.168.0.102:5173");
                        corsConfig.addAllowedOrigin("http://172.19.176.1:5173");
                        corsConfig.addAllowedMethod("*");
                        corsConfig.addAllowedHeader("*");
                        corsConfig.setAllowCredentials(true); // Set credentials to true
                        return corsConfig;
                    });
                })
                .httpBasic(AbstractHttpConfigurer::disable)
                .sessionManagement(sess->sess.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
                .exceptionHandling(exceptionHandling->exceptionHandling
                        .authenticationEntryPoint((request, response, authException) -> {
                            response.setStatus(HttpStatus.UNAUTHORIZED.value());
                            response.getWriter().write("Unauthorized");
                        })
                        .accessDeniedHandler(((request, response, accessDeniedException) -> {
                            response.setStatus(HttpStatus.FORBIDDEN.value());;
                            response.getWriter().write("Unauthorized");
                        })))
                .authorizeHttpRequests(auth->auth
                                .requestMatchers("/api/v1/auth/**").permitAll()
                                .requestMatchers(HttpMethod.GET,"/api/v1/addresses").permitAll()
                                .requestMatchers(HttpMethod.POST,"/api/v1/addresses").hasAuthority("ROLE_ADMIN")
                                .requestMatchers(HttpMethod.GET,"/api/v1/addresses/{id}").permitAll()
                                .requestMatchers(HttpMethod.PUT,"/api/v1/addresses/{id}").hasAnyAuthority("ROLE_ADMIN","ROLE_MUNICIPAL_SERVICE_OPERATING_ORGANIZATION")
                                .requestMatchers(HttpMethod.DELETE,"/api/v1/addresses/{id}").hasAnyAuthority("ROLE_ADMIN","ROLE_MUNICIPAL_SERVICE_OPERATING_ORGANIZATION")
                                .requestMatchers(HttpMethod.PATCH,"/api/v1/addresses/{id}").hasAnyAuthority("ROLE_ADMIN","ROLE_MUNICIPAL_SERVICE_OPERATING_ORGANIZATION")
                                .requestMatchers(HttpMethod.GET,"/api/v1/addresses/{id}/appeals").permitAll()
                                .requestMatchers(HttpMethod.PUT,"/api/v1/addresses/{id}/appeals").hasAuthority("ROLE_MUNICIPAL_SERVICE_OPERATING_ORGANIZATION")
                                .requestMatchers(HttpMethod.DELETE,"/api/v1/addresses/{id}/appeals").hasAuthority("ROLE_MUNICIPAL_SERVICE_OPERATING_ORGANIZATION")
                                .requestMatchers(HttpMethod.PATCH,"/api/v1/addresses/{id}/appeals").hasAuthority("ROLE_MUNICIPAL_SERVICE_OPERATING_ORGANIZATION")
                                .requestMatchers(HttpMethod.PATCH,"/api/v1/addresses/{id}/appeals/{propertyId}").hasAuthority("ROLE_MUNICIPAL_SERVICE_OPERATING_ORGANIZATION")
                                .requestMatchers(HttpMethod.DELETE,"/api/v1/addresses/{id}/appeals/{propertyId}").hasAuthority("ROLE_MUNICIPAL_SERVICE_OPERATING_ORGANIZATION")
                                .requestMatchers(HttpMethod.GET,"/api/v1/addresses/{id}/city").permitAll()
                                .requestMatchers(HttpMethod.GET,"/api/v1/addresses/{id}/city").hasAuthority("ROLE_ADMIN")
                                .requestMatchers(HttpMethod.DELETE,"/api/v1/addresses/{id}/city").hasAuthority("ROLE_ADMIN")
                                .requestMatchers(HttpMethod.GET,"/api/v1/addresses/{id}/city/{propertyId}").hasAuthority("ROLE_ADMIN")
                                .requestMatchers(HttpMethod.DELETE,"/api/v1/addresses/{id}/city/{propertyId}").hasAuthority("ROLE_ADMIN")
                                .requestMatchers(HttpMethod.GET,"/api/v1/addresses/{id}/news").permitAll()
                                .requestMatchers(HttpMethod.PUT,"/api/v1/addresses/{id}/news").hasAuthority("ROLE_MUNICIPAL_SERVICE_OPERATING_ORGANIZATION")
                                .requestMatchers(HttpMethod.DELETE,"/api/v1/addresses/{id}/news").hasAuthority("ROLE_MUNICIPAL_SERVICE_OPERATING_ORGANIZATION")
                                .requestMatchers(HttpMethod.PATCH,"/api/v1/addresses/{id}/news").hasAuthority("ROLE_MUNICIPAL_SERVICE_OPERATING_ORGANIZATION")
                                .requestMatchers(HttpMethod.GET,"/api/v1/addresses/{id}/news/{propertyId}").hasAuthority("ROLE_MUNICIPAL_SERVICE_OPERATING_ORGANIZATION")
                                .requestMatchers(HttpMethod.DELETE,"/api/v1/addresses/{id}/news/{propertyId}").hasAuthority("ROLE_MUNICIPAL_SERVICE_OPERATING_ORGANIZATION")
                                .requestMatchers(HttpMethod.GET,"/api/v1/appealses").permitAll()
                                .requestMatchers(HttpMethod.POST,"/api/v1/appealses").authenticated()
                                .requestMatchers(HttpMethod.GET,"/api/v1/appealses/{id}").permitAll()
                                .requestMatchers(HttpMethod.PUT,"/api/v1/appealses/{id}").hasAuthority("ROLE_MUNICIPAL_SERVICE_OPERATING_ORGANIZATION")
                                .requestMatchers(HttpMethod.DELETE,"/api/v1/appealses/{id}").hasAuthority("ROLE_MUNICIPAL_SERVICE_OPERATING_ORGANIZATION")
                                .requestMatchers(HttpMethod.PATCH,"/api/v1/appealses/{id}").hasAuthority("ROLE_MUNICIPAL_SERVICE_OPERATING_ORGANIZATION")
                                .requestMatchers(HttpMethod.GET,"/api/v1/appealses/{id}/address").permitAll()
                                .requestMatchers(HttpMethod.GET,"/api/v1/appealses/{id}/municipalModel").permitAll()
                                .requestMatchers(HttpMethod.GET,"/api/v1/appealses/{id}/user").permitAll()
                                .requestMatchers(HttpMethod.GET,"/api/v1/addresses/{id}/infrastructureObject").permitAll()
                                .requestMatchers(HttpMethod.GET,"/api/v1/cities").permitAll()
                                .requestMatchers(HttpMethod.GET,"/api/v1/cities/{id}").permitAll()
                                .requestMatchers(HttpMethod.GET,"/api/v1/cities/{id}/addresses").permitAll()
                                .requestMatchers(HttpMethod.GET,"/api/v1/cities/{id}/addresses/{propertyId}").permitAll()
                                .requestMatchers(HttpMethod.GET,"/api/v1/municipalModels").permitAll()
                                .requestMatchers(HttpMethod.GET,"/api/v1/municipalModels/{id}").permitAll()
                                .requestMatchers(HttpMethod.GET,"/api/v1/municipalModels/{id}/appealsList").permitAll()
                                .requestMatchers(HttpMethod.GET,"/api/v1/municipalModels/{id}/news").permitAll()
                                .requestMatchers(HttpMethod.GET,"/api/v1/news").permitAll()
                                .requestMatchers(HttpMethod.GET,"/api/v1/news/{id}").permitAll()
                                .requestMatchers(HttpMethod.GET,"/api/v1/news/{id}/address").permitAll()
                                .requestMatchers(HttpMethod.GET,"/api/v1/news/{id}/municipalModel").permitAll()
                                .requestMatchers(HttpMethod.GET,"/api/v1/news/{id}/surveys").permitAll()
                                .requestMatchers(HttpMethod.GET,"/api/v1/news/{id}/user").permitAll()
                                .requestMatchers(HttpMethod.GET,"/api/v1/surveys").permitAll()
                                .requestMatchers(HttpMethod.GET,"/api/v1/surveys/{id}").permitAll()
                                .requestMatchers(HttpMethod.GET,"/api/v1/surveys/{id}/news").permitAll()
                                .requestMatchers(HttpMethod.GET,"/api/v1/surveys/{id}/userList").permitAll()
                                .requestMatchers(HttpMethod.GET,"/api/v1/users").permitAll()
                                .requestMatchers(HttpMethod.GET,"/api/v1/users/{id}/appeals").permitAll()
                                .requestMatchers(HttpMethod.GET,"/api/v1/users/{id}/news").permitAll()
                                .requestMatchers(HttpMethod.GET,"/api/v1/users/{id}/surveys").permitAll()
                                .requestMatchers("/swagger-ui/**").permitAll()
                                .requestMatchers("v3/api-docs/**").permitAll()
                                .anyRequest().authenticated()
                        )
                .addFilterBefore(new JwtTokenFilter(tokenProvider), UsernamePasswordAuthenticationFilter.class);
        return httpSecurity.build();

    }
}

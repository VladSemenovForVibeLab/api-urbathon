package ru.quantumEntanglement.urbathon.service.interf;

import ru.quantumEntanglement.urbathon.dto.authentication.JWTRequest;
import ru.quantumEntanglement.urbathon.dto.authentication.JWTResponse;

public interface AuthService {
    JWTResponse login(JWTRequest loginRequest);

    JWTResponse refresh(String refreshToken);
}

package ru.quantumEntanglement.urbathon.service.interf;

import org.springframework.security.core.Authentication;
import ru.quantumEntanglement.urbathon.domain.User;
import ru.quantumEntanglement.urbathon.dto.user.UserDto;

import java.util.List;

public interface UserService {
    UserDto findById(Long id);
    UserDto updateEntity(UserDto entityDto);
    void createEntity(UserDto entityDto);
    User getByEmail(String email);
    UserDto getUserDtoByEmail(String email);
    List<String> findUserRoles(Authentication authentication);
}

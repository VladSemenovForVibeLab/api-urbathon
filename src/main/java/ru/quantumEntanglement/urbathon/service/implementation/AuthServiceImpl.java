package ru.quantumEntanglement.urbathon.service.implementation;

import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;
import ru.quantumEntanglement.urbathon.domain.User;
import ru.quantumEntanglement.urbathon.dto.authentication.JWTRequest;
import ru.quantumEntanglement.urbathon.dto.authentication.JWTResponse;
import ru.quantumEntanglement.urbathon.security.JwtTokenProvider;
import ru.quantumEntanglement.urbathon.service.interf.AuthService;
import ru.quantumEntanglement.urbathon.service.interf.UserService;

@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {
    private final AuthenticationManager authenticationManager;
    private final UserService userService;
    private final JwtTokenProvider jwtTokenProvider;
    @Override
    public JWTResponse login(JWTRequest loginRequest) {
        JWTResponse jwtResponse = new JWTResponse();
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getEmail(),loginRequest.getPassword()));
        User user = userService.getByEmail(loginRequest.getEmail());
        jwtResponse.setId(user.getId());
        jwtResponse.setEmail(user.getEmail());
        jwtResponse.setUsername(user.getUsername());
        jwtResponse.setAccessToken(jwtTokenProvider.createAccessToken(user.getId(),
                user.getEmail(),
                user.getRoles()));
        jwtResponse.setRefreshToken(jwtTokenProvider.createRefreshToken(user.getId(),
                user.getEmail()));
        return jwtResponse;
    }

    @Override
    public JWTResponse refresh(String refreshToken) {
        return jwtTokenProvider.refreshUserTokens(refreshToken);
    }
}

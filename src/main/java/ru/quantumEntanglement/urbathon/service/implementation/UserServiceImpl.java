package ru.quantumEntanglement.urbathon.service.implementation;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.quantumEntanglement.urbathon.domain.User;
import ru.quantumEntanglement.urbathon.domain.enums.Role;
import ru.quantumEntanglement.urbathon.dto.user.UserDto;
import ru.quantumEntanglement.urbathon.exception.ResourceNotFoundException;
import ru.quantumEntanglement.urbathon.repository.UserRepository;
import ru.quantumEntanglement.urbathon.service.interf.UserService;
import ru.quantumEntanglement.urbathon.util.UserDtoUtil;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final UserDtoUtil userDtoUtil;

    @Override
    public UserDto findById(Long id) {
        User entityFind = userRepository.findById(id).orElseThrow(()->new ResourceNotFoundException("Данная модель не найдена!"));
        return userDtoUtil.toDto(entityFind);

    }

    @Override
    public UserDto updateEntity(UserDto entityDto) {
        User user = userDtoUtil.toEntity(entityDto);
        user.setPassword(passwordEncoder.encode(entityDto.getPassword()));
        List<Role> roles = List.of(Role.ROLE_CITY_DWELLER);
        user.setRoles(roles);
        User entitySave = userRepository.save(user);
        UserDto dto = userDtoUtil.toDto(entitySave);
        return dto;
    }

    @Override
    @Transactional
    public void createEntity(UserDto userDto) {
        if(userRepository.findUserByEmail(userDto.getEmail()).isPresent()){
            throw new IllegalStateException("Пользователь уже зарегистрирован!.");
        }
        if(!userDto.getPassword().equals(userDto.getPasswordConfirmation())){
            throw new IllegalStateException("Пароль и подтверждение пароля не совпадают!");
        }
        User user = userDtoUtil.toEntity(userDto);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        List<Role> roles = List.of(Role.ROLE_CITY_DWELLER);
        user.setRoles(roles);
        userRepository.save(user);

    }

    @Override
    public User getByEmail(String email) {
        return userRepository.findUserByEmail(email)
                .orElseThrow(()->new ResourceNotFoundException("Пользователь не найден"));
    }

    @Override
    public UserDto getUserDtoByEmail(String email) {
        User user =  userRepository.findUserByEmail(email)
                .orElseThrow(()->new ResourceNotFoundException("Пользователь не найден"));
        UserDto userDto = userDtoUtil.toDto(user);
        return userDto;
    }

    @Override
    public List<String> findUserRoles(Authentication authentication) {
        List<String> userRoles = authentication.getAuthorities()
                .stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());
        return userRoles;
    }
}

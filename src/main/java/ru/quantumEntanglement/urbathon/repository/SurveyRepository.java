package ru.quantumEntanglement.urbathon.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import ru.quantumEntanglement.urbathon.domain.Survey;

@RepositoryRestResource
public interface SurveyRepository extends JpaRepository<Survey, Long> {
}
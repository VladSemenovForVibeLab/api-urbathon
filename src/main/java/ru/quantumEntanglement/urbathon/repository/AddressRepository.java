package ru.quantumEntanglement.urbathon.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import ru.quantumEntanglement.urbathon.domain.Address;

@RepositoryRestResource
public interface AddressRepository extends JpaRepository<Address, Long> {
}
package ru.quantumEntanglement.urbathon.domain;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.proxy.HibernateProxy;
import ru.quantumEntanglement.urbathon.domain.enums.Role;
import ru.quantumEntanglement.urbathon.domain.enums.SurveyAnswer;

import java.util.List;
import java.util.Objects;

@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "survey_tb")
@Setter
@Schema(description = "Модель опроса")
@Getter
public class Survey {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter(AccessLevel.NONE)
    @Column(name = "id")
    @Schema(description = "Id опроса",example = "1")
    private Long id;
    @ManyToOne(fetch = FetchType.EAGER)
    private News news;
    @Column(name = "question",nullable = false,length = 3000)
    @Schema(description = "Сам вопрос",example = "Был ли заменен светофор?")
    private String question;
    @Column(name = "survey_answer")
    @Enumerated(value = EnumType.STRING)
    private SurveyAnswer surveyAnswer;
    @ManyToMany
    @JoinTable(
            name = "survey_users",
            joinColumns = @JoinColumn(name = "survey_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id")
    )
    private List<User> userList;

    @Override
    public final boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        Class<?> oEffectiveClass = o instanceof HibernateProxy ? ((HibernateProxy) o).getHibernateLazyInitializer().getPersistentClass() : o.getClass();
        Class<?> thisEffectiveClass = this instanceof HibernateProxy ? ((HibernateProxy) this).getHibernateLazyInitializer().getPersistentClass() : this.getClass();
        if (thisEffectiveClass != oEffectiveClass) return false;
        Survey survey = (Survey) o;
        return getId() != null && Objects.equals(getId(), survey.getId());
    }

    @Override
    public final int hashCode() {
        return this instanceof HibernateProxy ? ((HibernateProxy) this).getHibernateLazyInitializer().getPersistentClass().hashCode() : getClass().hashCode();
    }
}

package ru.quantumEntanglement.urbathon.domain;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.proxy.HibernateProxy;
import ru.quantumEntanglement.urbathon.domain.enums.RequestStatus;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "appeals_tb")
@Entity
@Schema(description = "Обращение граждан")
public class Appeals implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @Schema(description = "Id обращения",example = "1")
    @Setter(AccessLevel.NONE)
    private Long id;
    @Column(name = "text_appeals")
    @Schema(description = "Текст данного обращения",example = "Отсутствие уличного освещения")
    private String textAppeals;
    @ManyToOne(fetch = FetchType.EAGER)
    private Address address;
    @ManyToOne(fetch = FetchType.EAGER)
    private User user;
    @ManyToOne(fetch = FetchType.EAGER)
    private MunicipalModel municipalModel;
    @Column(name = "request_status")
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "appeals_request_statuses")
    @Enumerated(value = EnumType.STRING)
    private List<RequestStatus> requestStatusList;

    @Override
    public final boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        Class<?> oEffectiveClass = o instanceof HibernateProxy ? ((HibernateProxy) o).getHibernateLazyInitializer().getPersistentClass() : o.getClass();
        Class<?> thisEffectiveClass = this instanceof HibernateProxy ? ((HibernateProxy) this).getHibernateLazyInitializer().getPersistentClass() : this.getClass();
        if (thisEffectiveClass != oEffectiveClass) return false;
        Appeals appeals = (Appeals) o;
        return getId() != null && Objects.equals(getId(), appeals.getId());
    }

    @Override
    public final int hashCode() {
        return this instanceof HibernateProxy ? ((HibernateProxy) this).getHibernateLazyInitializer().getPersistentClass().hashCode() : getClass().hashCode();
    }
}

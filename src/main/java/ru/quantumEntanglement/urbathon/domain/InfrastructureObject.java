package ru.quantumEntanglement.urbathon.domain;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Entity
@Table(name = "infrastructure_object")
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Schema(description = "Модель объекта инфраструктуры")
public class InfrastructureObject {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @Schema(description = "Id модели объекта инфраструктуры",example = "1")
    @Setter(AccessLevel.NONE)
    private Long id;
    @Schema(description = "Именование объекта инфраструктуры")
    @Column(name = "name")
    private String nameOfTheInfrastructureObject;
    @Schema(description = "Широта",example = "55.751244")
    @Column(name = "latitude")
    private String latitude;
    @Schema(description = "Долгота",example = "37.618423")
    @Column(name = "longitude")
    private String longitude;
    @ManyToMany
    @JoinTable(
            name = "infrastructure_objects_municipal_models",
            joinColumns = @JoinColumn(name = "infrastructure_object_id"),
            inverseJoinColumns = @JoinColumn(name = "municipal_model_id")
    )
    List<MunicipalModel> municipalModels;
    @ManyToOne(fetch = FetchType.EAGER)
    private City city;
    @OneToOne(mappedBy = "infrastructureObject")
    private Address address;

}

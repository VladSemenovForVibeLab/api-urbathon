package ru.quantumEntanglement.urbathon.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.proxy.HibernateProxy;
import ru.quantumEntanglement.urbathon.domain.enums.Role;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "user_tb")
@Schema(description = "Модель пользователя")
public  class User implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter(AccessLevel.NONE)
    @Schema(description = "Id пользователя",example = "1")
    private Long id;
    @Column(name = "city",nullable = true)
    @Schema(description = "Город проживания",example = "Москва")
    private String city;
    @Schema(description = "Контактный номер пользователя",example = "89999999999")
    @Column(name = "contact_number",nullable = false,unique = true)
    private String contactNumber;
    @Column(name = "username")
    @Schema(description = "Имя пользователя",example = "Vlad")
    private String username;
    @Column(name = "lastname")
    @Schema(description = "User lastname",example = "Semenov")
    private String lastname;
    @Column(name = "email")
    @Email
    @Schema(description = "User email",example = "ooovladislavchik@gmail.com")
    private String email;
    @Column(name = "date_of_birth")
    private LocalDate dateOfBirth;
    @Transient
    private int age;
    @Transient
    private String passwordConfirmation;
    @Column(name = "password",length = 3000,nullable = false)
    private String password;
    @JsonFormat(pattern = "yyyy-mm-dd HH:mm:ss")
    @Column(name = "create_date")
    @CreationTimestamp
    private LocalDateTime createDate;
    @Column(name = "role")
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "users_roles")
    @Enumerated(value = EnumType.STRING)
    private List<Role> roles;
    @UpdateTimestamp
    @Column(name = "update_date",nullable = false)
    private LocalDateTime updateDate;
    @OneToMany(mappedBy = "user")
    private List<Appeals> appeals;
    @OneToMany(mappedBy = "user")
    private List<News> news;
    @OneToMany(mappedBy = "user")
    private List<Event> events;
    @ManyToMany(mappedBy = "userList")
    private List<Survey> surveys;

    @Override
    public final boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        Class<?> oEffectiveClass = o instanceof HibernateProxy ? ((HibernateProxy) o).getHibernateLazyInitializer().getPersistentClass() : o.getClass();
        Class<?> thisEffectiveClass = this instanceof HibernateProxy ? ((HibernateProxy) this).getHibernateLazyInitializer().getPersistentClass() : this.getClass();
        if (thisEffectiveClass != oEffectiveClass) return false;
        User user = (User) o;
        return getId() != null && Objects.equals(getId(), user.getId());
    }

    @Override
    public final int hashCode() {
        return this instanceof HibernateProxy ? ((HibernateProxy) this).getHibernateLazyInitializer().getPersistentClass().hashCode() : getClass().hashCode();
    }
}

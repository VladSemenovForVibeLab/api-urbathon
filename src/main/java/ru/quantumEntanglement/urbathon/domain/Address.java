package ru.quantumEntanglement.urbathon.domain;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.proxy.HibernateProxy;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "address_tb")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Schema(description = "Модель адреса ")
public class Address implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @Setter(AccessLevel.NONE)
    @Schema(description = "Adress id",example = "1")
    private Long id;
    @ManyToOne(fetch = FetchType.EAGER)
    @Schema(description = "Город",example = "Moscow")
    private City city;
    @OneToMany(mappedBy = "address",fetch = FetchType.LAZY)
    private List<Appeals> appeals;
    @OneToMany(mappedBy = "address",fetch = FetchType.LAZY)
    private List<News> news;
    @Schema(description = "Улица",example = "ул. Ленина")
    @Column(name = "street")
    private String street;
    @Schema(description = "Номер дома",example = "12")
    @Column(name = "house")
    private Integer house;
    @Schema(description = "Широта",example = "55.751244")
    @Column(name = "latitude")
    private String latitude;
    @Schema(description = "Долгота",example = "37.618423")
    @Column(name = "longitude")
    private String longitude;
    @OneToOne(cascade = CascadeType.ALL)
    private InfrastructureObject infrastructureObject;

    @Override
    public final boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        Class<?> oEffectiveClass = o instanceof HibernateProxy ? ((HibernateProxy) o).getHibernateLazyInitializer().getPersistentClass() : o.getClass();
        Class<?> thisEffectiveClass = this instanceof HibernateProxy ? ((HibernateProxy) this).getHibernateLazyInitializer().getPersistentClass() : this.getClass();
        if (thisEffectiveClass != oEffectiveClass) return false;
        Address address = (Address) o;
        return getId() != null && Objects.equals(getId(), address.getId());
    }

    @Override
    public final int hashCode() {
        return this instanceof HibernateProxy ? ((HibernateProxy) this).getHibernateLazyInitializer().getPersistentClass().hashCode() : getClass().hashCode();
    }
}

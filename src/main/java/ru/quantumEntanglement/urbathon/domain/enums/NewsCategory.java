package ru.quantumEntanglement.urbathon.domain.enums;

public enum NewsCategory {
    WATER,
    LIGHT,
    GAS,
    HEATING,
    STREET,
    OTHER
}

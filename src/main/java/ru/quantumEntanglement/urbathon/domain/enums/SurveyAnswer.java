package ru.quantumEntanglement.urbathon.domain.enums;

import lombok.Getter;

@Getter
public enum SurveyAnswer {
    YES_IT_WAS_DONE,
    NO_IT_WAS_NOT_FULFILLED,
    PARTIALLY_COMPLETED,
    NOT_COMPLETED_YET_BUT_PLANNED,
    I_DONT_KNOW_CANT_ANSWER
}

package ru.quantumEntanglement.urbathon.domain.enums;

public enum Role {
    ROLE_CITY_DWELLER,
    ROLE_MUNICIPAL_SERVICE_OPERATING_ORGANIZATION,
    ROLE_ADMIN

}

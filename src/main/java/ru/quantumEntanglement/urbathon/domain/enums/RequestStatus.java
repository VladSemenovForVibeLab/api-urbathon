package ru.quantumEntanglement.urbathon.domain.enums;

public enum RequestStatus {
    NEW,
    PROCESSING,
    IN_PROGRESS,
    COMPLETED,
    REJECTED,
    INFORMATION_AWAITED,
    PENDING_RESOLUTION,
    IN_THE_ARCHIVE
}

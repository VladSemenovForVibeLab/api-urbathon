package ru.quantumEntanglement.urbathon.domain;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import jakarta.validation.constraints.Size;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Service
@Entity
@Table(name = "event_tb")
@Schema(description = "Модель события")
public class Event {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @Schema(description = "Id события",example = "1")
    @Setter(AccessLevel.NONE)
    private Long id;
    @Column(name = "title_event")
    @Schema(description = "Название события")
    @Size(min = 2,max = 100)
    private String titleEvent;
    @Schema(description = "тело события - его описание")
    @Column(name = "body",length = 3000)
    private String body;
    @Schema(description = "Широта",example = "55.751244")
    @Column(name = "latitude")
    private String latitude;
    @Schema(description = "Долгота",example = "37.618423")
    @Column(name = "longitude")
    private String longitude;
    @Column(name = "create_date")
    @Schema(description = "Дата начала")
    @CreationTimestamp
    private LocalDateTime createDate;
    @Column(name = "end date")
    @Schema(description = "Дата окончания")
    private LocalDateTime end_date;
    @ManyToOne(fetch = FetchType.EAGER)
    private User user;
}

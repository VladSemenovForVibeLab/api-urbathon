package ru.quantumEntanglement.urbathon.domain;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.proxy.HibernateProxy;
import ru.quantumEntanglement.urbathon.domain.enums.NewsCategory;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(name = "news_tb")
@Entity
@Schema(description = "Модель новости")
public class News implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter(AccessLevel.NONE)
    @Column(name = "id")
    @Schema(description = "Id новости",example = "1")
    private Long id;
    @Column(name = "heading", length = 500, nullable = false)
    @Size(min = 2, max = 500)
    @Schema(description = "Заголовок новости",example = "Реконструкция парка \"Зеленый остров\"")
    @NotBlank(message = "Заголовок новости не может быть пустым")
    private String heading;
    @Column(name = "news_category")
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "news_news_categories")
    @Enumerated(value = EnumType.STRING)
    private List<NewsCategory> newsCategories;
    @Column(name = "text_news")
    @Schema(description = "Основной текст новости",example = "В ближайшие месяцы начнется реконструкция любимого парка горожан")
    private String textNews;
    @CreationTimestamp
    @Column(name = "create_date_for_news", nullable = false)
    private LocalDateTime createDateForNews;
    @ManyToOne(fetch = FetchType.EAGER)
    private Address address;
    @ManyToOne(fetch = FetchType.EAGER)
    private MunicipalModel municipalModel;
    @Column(name = "image_url")
    @Schema(description = "Картинка к новости -> url ведущая к ней",example = "image1.jpg")
    private String imageUrl;
    @ManyToOne(fetch = FetchType.EAGER)
    private User user;
    @OneToMany(mappedBy = "news")
    private List<Survey> surveys;
    @Override
    public final boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        Class<?> oEffectiveClass = o instanceof HibernateProxy ? ((HibernateProxy) o).getHibernateLazyInitializer().getPersistentClass() : o.getClass();
        Class<?> thisEffectiveClass = this instanceof HibernateProxy ? ((HibernateProxy) this).getHibernateLazyInitializer().getPersistentClass() : this.getClass();
        if (thisEffectiveClass != oEffectiveClass) return false;
        News news = (News) o;
        return getId() != null && Objects.equals(getId(), news.getId());
    }

    @Override
    public final int hashCode() {
        return this instanceof HibernateProxy ? ((HibernateProxy) this).getHibernateLazyInitializer().getPersistentClass().hashCode() : getClass().hashCode();
    }
}

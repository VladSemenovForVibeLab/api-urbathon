package ru.quantumEntanglement.urbathon.domain;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.proxy.HibernateProxy;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "municipal_model_tb")
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Schema(description = "Модель муниципального образования")
public class MunicipalModel implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter(AccessLevel.NONE)
    @Column(name = "id")
    @Schema(description = "Id муниципального образования",example = "1")
    private Long id;
    @Column(name = "nameMunicipalService")
    @Schema(description = "Наименование муниципального образования",example = "'Федеральная служба по надзору в сфере защиты прав потребителей и благополучия человека")
    private String nameMunicipalService;
    @OneToMany(mappedBy = "municipalModel")
    private List<Appeals> appealsList;
    @OneToMany(mappedBy = "municipalModel")
    private List<News> news;
    @ManyToMany(mappedBy = "municipalModels")
    private List<InfrastructureObject> infrastructureObjects;

    @Override
    public final boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        Class<?> oEffectiveClass = o instanceof HibernateProxy ? ((HibernateProxy) o).getHibernateLazyInitializer().getPersistentClass() : o.getClass();
        Class<?> thisEffectiveClass = this instanceof HibernateProxy ? ((HibernateProxy) this).getHibernateLazyInitializer().getPersistentClass() : this.getClass();
        if (thisEffectiveClass != oEffectiveClass) return false;
        MunicipalModel that = (MunicipalModel) o;
        return getId() != null && Objects.equals(getId(), that.getId());
    }

    @Override
    public final int hashCode() {
        return this instanceof HibernateProxy ? ((HibernateProxy) this).getHibernateLazyInitializer().getPersistentClass().hashCode() : getClass().hashCode();
    }
}

package ru.quantumEntanglement.urbathon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UrbathonApplication {

	public static void main(String[] args) {
		SpringApplication.run(UrbathonApplication.class, args);
	}

}

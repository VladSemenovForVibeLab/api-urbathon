package ru.quantumEntanglement.urbathon.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.quantumEntanglement.urbathon.dto.authentication.JWTRequest;
import ru.quantumEntanglement.urbathon.dto.authentication.JWTResponse;
import ru.quantumEntanglement.urbathon.dto.response.MessageResponse;
import ru.quantumEntanglement.urbathon.dto.user.UserDto;
import ru.quantumEntanglement.urbathon.service.interf.AuthService;
import ru.quantumEntanglement.urbathon.service.interf.UserService;

@RestController
@RequestMapping("/api/v1/auth")
@RequiredArgsConstructor
@Validated
@Tag(name = "Authentication Controller",description = "Authentication API")
public class AuthController {
    private final AuthService authService;
    private final UserService userService;

    @PostMapping("/login")
    public JWTResponse login(@Validated @RequestBody JWTRequest loginRequest){
        return authService.login(loginRequest);
    }

    @PostMapping("/register")
    public ResponseEntity<MessageResponse> register(@Validated @RequestBody UserDto userDto){
        userService.createEntity(userDto);
        return new ResponseEntity<>(new MessageResponse("Пользователь зарегистрирован!"), HttpStatus.CREATED);
    }

    @PostMapping("/refresh")
    public JWTResponse refresh(@RequestBody String refreshToken){
        return authService.refresh(refreshToken);
    }

}


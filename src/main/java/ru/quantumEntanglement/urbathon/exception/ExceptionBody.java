package ru.quantumEntanglement.urbathon.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.HttpStatus;

import java.util.Map;

/**
 * Класс ExceptionBody используется для передачи информации об исключении или ошибке.
 * Содержит сообщение об ошибке и, возможно, словарь ошибок, связанных с конкретными полями или параметрами.
 */

@Data
@AllArgsConstructor
public class ExceptionBody {
    /**
     * Сообщение об ошибке.
     */
    private String message;
    /**
     * Словарь ошибок, связанных с конкретными полями или параметрами.
     * Ключ словаря представляет имя поля или параметра, а значение - текстовое описание ошибки.
     */
    private Map<String,String> errors;
    private Map<HttpStatus,String> errorsWithStatus;

    public ExceptionBody(Map<HttpStatus, String> errorsWithStatus) {
        this.errorsWithStatus = errorsWithStatus;
    }

    public ExceptionBody(String message, Map<String, String> errors) {
        this.message = message;
        this.errors = errors;
    }
    /**
     * Конструктор класса ExceptionBody с параметрами.
     * @param message сообщение об ошибке.
     */
    public ExceptionBody(String message) {
        this.message = message;
    }
}

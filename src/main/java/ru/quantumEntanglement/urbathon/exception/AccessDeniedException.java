package ru.quantumEntanglement.urbathon.exception;


/**
 * Исключение, выбрасываемое при отказе в доступе.
 */
public class AccessDeniedException extends RuntimeException{
    public AccessDeniedException() {
        super();
    }
}

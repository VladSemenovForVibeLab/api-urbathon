package ru.quantumEntanglement.urbathon.dto.authentication;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
@Schema(description = "Запрос для аутентификации")
public class JWTRequest {
    @Schema(description = "email",example = "ooovladislavchik@gmail.com")
    @NotNull(message = "Почта не должна быть пуста!")
    private String email;
    @Schema(description = "Пароль для входа в систему",example = "1q2w3e")
    @NotNull(message = "Пароль не должен быть пустым")
    private String password;
}
